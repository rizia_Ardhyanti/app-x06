package ardhyanti.rizia.appx06


import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragPerkalian: FragmentPerkalian
    lateinit var fragMatkul : FragmentMatkul
    lateinit var fragNilai: FragmentNilai
    lateinit var  ft : FragmentTransaction
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragPerkalian = FragmentPerkalian()
        fragMatkul = FragmentMatkul()
        fragNilai = FragmentNilai()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProdi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMhs).commit()
                frameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            /*R.id.itemPolinema ->{
                var webUri = "https://polinema.ac.id"
                var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                startActivity(intentInternet)
            }

             */
            /*R.id.itemPerkalian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragPerkalian).commit()
                frameLayout.setBackgroundColor(Color.argb(245,240,248,254))
                frameLayout.visibility = View.VISIBLE
            }

             */
            R.id.itemMatkul ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMatkul).commit()
                frameLayout.setBackgroundColor(Color.argb(245,230,230,250))
                frameLayout.visibility = View.VISIBLE
            }

            R.id.itemNilai ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragNilai).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,248,220))
                frameLayout.visibility = View.VISIBLE
            }

            R.id.itemAbout -> frameLayout.visibility = View.GONE
        }
        return true
    }
}
